package it.milkazardevs.poly.enums;

public enum Alimentazione {
	CARNIVORO, ERBIVORO, ONNIVORO;
}
