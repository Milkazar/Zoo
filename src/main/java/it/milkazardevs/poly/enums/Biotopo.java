package it.milkazardevs.poly.enums;

public enum Biotopo {
	TERRA, ACQUA, TERRACQUA, ARIA;
}
