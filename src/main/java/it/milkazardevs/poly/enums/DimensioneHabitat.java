package it.milkazardevs.poly.enums;

public enum DimensioneHabitat {
	PICCOLO(5),
	MEDIO(12),
	GRANDE(20);

	private int popolazione;

	private DimensioneHabitat(int popolazione) {
		this.popolazione = popolazione;
	}

	public int getPopolazione() {
		return this.popolazione;
	}
}
