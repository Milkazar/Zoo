package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;
import lombok.Data;

@Data
public abstract class Animale {

    protected Biotopo biotopo;
    protected Alimentazione alimentazione;

    public Animale(Alimentazione alimentazione, Biotopo biotopo) {
        this.alimentazione = alimentazione;
        this.biotopo = biotopo;
    }

}
