package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;

public abstract class Anfibio extends Animale {
    public Anfibio(Alimentazione alimentazione) {
        super(alimentazione, Biotopo.TERRACQUA);
    }
}
