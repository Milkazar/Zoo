package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Capra extends Terrestre {

    public Capra() {
        super(Alimentazione.ERBIVORO);
    }
}
