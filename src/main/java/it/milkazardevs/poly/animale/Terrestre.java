package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;

public abstract class Terrestre extends Animale {
    public Terrestre(Alimentazione alimentazione) {
        super(alimentazione, Biotopo.TERRA);
    }
}
