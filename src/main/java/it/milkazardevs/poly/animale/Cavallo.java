package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Cavallo extends Terrestre {

    public Cavallo() {
        super(Alimentazione.ERBIVORO);
    }
}
