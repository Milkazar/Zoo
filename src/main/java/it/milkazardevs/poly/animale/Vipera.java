package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Vipera extends Terrestre {

    public Vipera() {
        super(Alimentazione.CARNIVORO);
    }
}
