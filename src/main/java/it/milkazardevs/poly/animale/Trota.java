package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Trota extends Acquatico {

    public Trota() {
        super(Alimentazione.ERBIVORO);
    }
}
