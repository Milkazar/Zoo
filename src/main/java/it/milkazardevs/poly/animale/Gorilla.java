package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Gorilla extends Terrestre {

    public Gorilla() {
        super(Alimentazione.ONNIVORO);
    }
}
