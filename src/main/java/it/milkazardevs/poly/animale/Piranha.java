package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Piranha extends Acquatico {

    public Piranha() {
        super(Alimentazione.CARNIVORO);
    }
}
