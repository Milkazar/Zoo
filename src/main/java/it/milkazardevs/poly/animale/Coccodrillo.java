package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Coccodrillo extends Anfibio {

    public Coccodrillo() {
        super(Alimentazione.CARNIVORO);
    }
}
