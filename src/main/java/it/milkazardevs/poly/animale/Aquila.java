package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class Aquila extends Volatile {

    public Aquila() {
        super(Alimentazione.CARNIVORO);
    }
}
