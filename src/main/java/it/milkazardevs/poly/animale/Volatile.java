package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;

public abstract class Volatile extends Animale{
    public Volatile(Alimentazione alimentazione) {
        super(alimentazione, Biotopo.ARIA);
    }
}
