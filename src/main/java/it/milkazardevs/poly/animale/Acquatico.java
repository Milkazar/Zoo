package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;

public abstract class Acquatico extends Animale {
    public Acquatico(Alimentazione alimentazione) {
        super(alimentazione, Biotopo.ACQUA);
    }
}
