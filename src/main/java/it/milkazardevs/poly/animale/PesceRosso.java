package it.milkazardevs.poly.animale;

import it.milkazardevs.poly.enums.Alimentazione;

public class PesceRosso extends Acquatico {

    public PesceRosso() {
        super(Alimentazione.ERBIVORO);
    }
}
