package it.milkazardevs.poly.habitat;

import it.milkazardevs.poly.enums.DimensioneHabitat;

public class Terrario extends Habitat {

	public Terrario(DimensioneHabitat dimensioneHabitat) {
		super(dimensioneHabitat);
	}
}
