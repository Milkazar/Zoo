package it.milkazardevs.poly.habitat;

import it.milkazardevs.poly.enums.DimensioneHabitat;

public class Gabbia extends Habitat {

	public Gabbia(DimensioneHabitat dimensioneHabitat) {
		super(dimensioneHabitat);
	}
}
