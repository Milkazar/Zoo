package it.milkazardevs.poly.habitat;

import it.milkazardevs.poly.animale.Animale;
import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;
import it.milkazardevs.poly.enums.DimensioneHabitat;
import lombok.Data;
import lombok.NonNull;

import java.util.Random;

@Data
public class HabitatFactory {

    public static Habitat createHabitat(Animale animale) {

        Random rand = new Random();
        DimensioneHabitat dimensioneHabitat =DimensioneHabitat.values()[rand.nextInt(DimensioneHabitat.values().length)];

        switch (animale.getBiotopo()){
            case TERRA:
                if(animale.getAlimentazione()== Alimentazione.ERBIVORO){
                    return new Recinto(dimensioneHabitat);
                }
                return new Gabbia(dimensioneHabitat);
            case ACQUA:
                return new Acquario(dimensioneHabitat);
            case TERRACQUA:
                return new Terrario(dimensioneHabitat);
            case ARIA:
                return new Gabbia(dimensioneHabitat);
            default: return null;
        }
    }

}
