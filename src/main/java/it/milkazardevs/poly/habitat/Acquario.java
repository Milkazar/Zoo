package it.milkazardevs.poly.habitat;

import it.milkazardevs.poly.enums.DimensioneHabitat;

public class Acquario extends Habitat {

	public Acquario(DimensioneHabitat dimensioneHabitat) {
		super(dimensioneHabitat);
	}
}
