package it.milkazardevs.poly.habitat;

import it.milkazardevs.poly.animale.Animale;
import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.enums.Biotopo;
import it.milkazardevs.poly.enums.DimensioneHabitat;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public abstract class Habitat {


    protected List<Animale> animali = new ArrayList<Animale>();
    protected DimensioneHabitat dimensione;

    public Habitat(DimensioneHabitat dimensioneHabitat) {
        this.dimensione = dimensioneHabitat;

    }

    public Alimentazione retrieveMainAlimentazione() {
        return animali.get(0).getAlimentazione();
    }

    public Biotopo retrieveMainBiotopo() {
        return animali.get(0).getBiotopo();
    }

    public String getAnimalTypes() {
        return animali.stream().map(animale -> animale.getClass().getSimpleName()).distinct().collect(Collectors.joining(", "));
    }

    public boolean hasSpace() {
        return this.dimensione.getPopolazione() != this.animali.size();
    }

    public boolean containsAnimal(final Animale animale) {
        return animali.stream().anyMatch(a -> animale.getClass().getSimpleName().equalsIgnoreCase(a.getClass().getSimpleName()));
    }
}
