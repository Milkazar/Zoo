package it.milkazardevs.poly.habitat;

import it.milkazardevs.poly.enums.DimensioneHabitat;

public class Recinto extends Habitat {

	public Recinto(DimensioneHabitat dimensioneHabitat) {
		super(dimensioneHabitat);
	}
}
