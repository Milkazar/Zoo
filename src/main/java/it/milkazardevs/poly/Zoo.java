package it.milkazardevs.poly;

import it.milkazardevs.poly.animale.*;
import it.milkazardevs.poly.enums.Alimentazione;
import it.milkazardevs.poly.habitat.Habitat;
import it.milkazardevs.poly.habitat.HabitatFactory;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Zoo {

    public static void main(String[] args) {
        try {
            new Zoo().creaZoo();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    List<Animale> animali;
    List<Habitat> habitats;
    Random rand = new Random();

    public void creaZoo() throws Exception, InstantiationException {
        this.popolaAnimali();

        this.distribuisciAnimali();

        this.stampaZoo();

    }

    private void stampaZoo() {

        Map<Class<?>, List<Animale>> map = this.animali.stream().sorted(Comparator.comparing(a -> a.getClass().getSimpleName())).collect(Collectors.groupingBy(Animale::getClass));
        map.entrySet().forEach(a -> System.out.println(a.getKey().getSimpleName() + ": " + a.getValue().size()));

        this.habitats.stream().sorted(Comparator.comparing(hh -> hh.getClass().getSimpleName())).forEach(h -> {
            System.out.println("L'habitat " + h.getClass().getSimpleName() + " contiene " + h.getAnimali().size() + " animali di tipo " + h.getAnimalTypes());
        });

    }

    private void distribuisciAnimali() {
        this.habitats = new ArrayList<>();

        for (Animale animale : this.animali) {
            Habitat habitat = this.ricavaGabbia(animale);
            habitat.getAnimali().add(animale);
        }

    }

    private Habitat ricavaGabbia(Animale animale) {


        Predicate<Habitat> erbivori = habitat -> animale.getAlimentazione() == Alimentazione.ERBIVORO && habitat.retrieveMainAlimentazione() == Alimentazione.ERBIVORO;
        Predicate<Habitat> carnivori = habitat -> animale.getAlimentazione() != Alimentazione.ERBIVORO && habitat.containsAnimal(animale);

        Habitat trovato = this.habitats.stream()
                .filter(Habitat::hasSpace)
                .filter(habitat -> habitat.retrieveMainBiotopo() == animale.getBiotopo())
                .filter(erbivori.or(carnivori))
                .findFirst()
                .orElse(null);

        if (trovato == null) {
            trovato = HabitatFactory.createHabitat(animale);
            this.habitats.add(trovato);
        }

        return trovato;
    }

    private void popolaAnimali() throws IllegalAccessException, InstantiationException {

        Class<?>[] animaliClazz = {Aquila.class, Capra.class, Cavallo.class, Coccodrillo.class, Gorilla.class, PesceRosso.class, Piranha.class, Trota.class, Vipera.class};

        this.animali = new ArrayList<>();

        for (int i = 0; i < 300; i++) {
            Class<?> clazz = animaliClazz[this.rand.nextInt(animaliClazz.length)];
            Animale obj = (Animale) clazz.newInstance();
            this.animali.add(obj);
        }

    }
}
